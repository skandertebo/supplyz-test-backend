import { connect } from 'mongoose';
import { CONNECTION_STRING } from './constants';

const connectionPromise = connect(CONNECTION_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

export const setupDbConnection = async () => {
  await connectionPromise;
  console.log('Connected to database');
};
