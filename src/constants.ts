import { config } from 'dotenv';

config();

export const weatherApiBaseUrl = 'https://api.tomorrow.io/v4/weather/forecast';
export const WEATHER_API_KEY = process.env.WEATHER_API_KEY;

export const CONNECTION_STRING = process.env.CONNECTION_STRING;
