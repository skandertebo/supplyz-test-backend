import app from './app';
import { setupDbConnection } from './db-connection';

const port = 4000;

async function main() {
  await setupDbConnection();

  app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });
}

main();
