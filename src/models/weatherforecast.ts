import * as mongoose from 'mongoose';
const WeatherForecastSchema = new mongoose.Schema({
  time: String,
  values: {
    temperatureAvg: Number
  },

  location: String
});

export const WeatherForecastModel = mongoose.model(
  'WeatherForecast',
  WeatherForecastSchema
);

export type WeatherForecast = {
  time: string;
  values: {
    temperatureAvg: number;
  };
  location: string;
};
