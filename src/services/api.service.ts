import axios from 'axios';
import { WEATHER_API_KEY, weatherApiBaseUrl } from '../constants';

const apiService = axios.create({
  baseURL: weatherApiBaseUrl
});

apiService.interceptors.request.use((cfg) => {
  cfg.headers['Accept'] = 'application/json';
  cfg.params = {
    ...cfg.params,
    apikey: WEATHER_API_KEY
  };
  return cfg;
});

export default apiService;
