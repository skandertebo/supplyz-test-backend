import ResponseException from '../../Exception/ResponseException';
import {
  WeatherForecast,
  WeatherForecastModel
} from '../../models/weatherforecast';
import { WeatherApiResponse } from '../../types/weather.types';
import XOR from '../../utils/xor';
import apiService from '../api.service';
export default class WeatherForecastService {
  static async QueryWeatherForecast(city: string) {
    const response = await apiService.get<WeatherApiResponse>('', {
      params: {
        location: city,
        fields: [
          'temperature',
          'temperatureApparent',
          'windSpeed',
          'windDirection',
          'humidity',
          'precipitationIntensity',
          'precipitationType',
          'cloudCover',
          'weatherCode'
        ].join(','),
        units: 'metric',
        timesteps: '1d',
        timezone: 'Tunisia/Tunis'
      }
    });
    return response.data;
  }

  static async getWeatherForecast5DaysMaxForward(
    city: string,
    startDate?: string,
    endDate?: string
  ) {
    const today = new Date();
    today.setUTCHours(0, 0, 0, 0);
    const fiveDaysFromNow = new Date();
    fiveDaysFromNow.setDate(today.getDate() + 4);
    if (XOR(startDate, endDate)) {
      throw new ResponseException(
        'You must provide both startDate and endDate',
        400
      );
    }
    if (new Date(startDate) < today) {
      throw new ResponseException('startDate must be in the future', 400);
    }
    // end date should be max 5 days from now
    if (new Date(endDate) > fiveDaysFromNow) {
      throw new ResponseException('endDate must be max 5 days from now', 400);
    }
    const elementsInDatabase = await WeatherForecastModel.find({
      location: city,
      time: {
        $gte: today.toISOString()
      }
    }).limit(5);
    const elementsInDatabaseCount = elementsInDatabase.length;
    if (elementsInDatabaseCount >= 5) {
      if (startDate && endDate) {
        const startDateObject = new Date(startDate);
        const endDateObject = new Date(endDate);
        endDateObject.setUTCHours(23, 59, 59, 999);
        return elementsInDatabase.filter(
          (element: WeatherForecast) =>
            new Date(element.time) >= startDateObject &&
            new Date(element.time) <= endDateObject
        );
      }
      return elementsInDatabase;
    } else {
      const weatherForecast = await this.QueryWeatherForecast(city);
      for (const element of weatherForecast.timelines.daily) {
        const existingElement = await WeatherForecastModel.findOne({
          location: city,
          time: element.time
        });
        if (!existingElement) {
          await WeatherForecastModel.create({
            location: city,
            time: element.time,
            values: {
              temperatureAvg: element.values.temperatureAvg
            }
          });
        } else {
          await WeatherForecastModel.updateOne(
            {
              location: city,
              time: element.time
            },
            {
              values: element.values
            }
          );
        }
      }
      const elements = await WeatherForecastModel.find({
        location: city,
        time: {
          $gte: today.toISOString()
        }
      }).limit(5);
      if (startDate && endDate) {
        return elements.filter(
          (element: WeatherForecast) =>
            new Date(element.time) >= new Date(startDate) &&
            new Date(element.time) <= new Date(endDate)
        );
      }
      return elements;
    }
  }
}
