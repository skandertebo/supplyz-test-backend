import { AxiosError } from 'axios';
import cors from 'cors';
import express from 'express';
import ResponseException from './Exception/ResponseException';
import WeatherForecastService from './services/WeatherForecast/weather-forecast.service';

const app = express();
app.use(cors());

app.get('/', (req, res) => {
  res.send('Hello world');
});

app.get('/forecast', async (req, res) => {
  const location = req.query.location;
  if (!location || typeof location !== 'string') {
    return res.status(400).send('Location is required');
  }
  const startDate = req.query.startDate;
  const endDate = req.query.endDate;
  if (typeof startDate !== 'string' || typeof endDate !== 'string') {
    if (startDate || endDate)
      return res.status(400).send('startDate and endDate must be strings');
  }
  try {
    const weatherForecast =
      await WeatherForecastService.getWeatherForecast5DaysMaxForward(
        location,
        startDate as string,
        endDate as string
      );
    return res.status(200).send(weatherForecast);
  } catch (error) {
    console.error(error);
    if (error instanceof AxiosError) {
      const axiosError = error as AxiosError;
      return res
        .status(axiosError.response?.status || 500)
        .send(axiosError.response?.data);
    }
    if (error instanceof ResponseException) {
      return res.status(error.status).send(error.message);
    }
    return res.status(500).send('Internal server error');
  }
});

export default app;
