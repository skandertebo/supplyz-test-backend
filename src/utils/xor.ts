export default function XOR(a: any, b: any): boolean {
  return (a || b) && !(a && b);
}
